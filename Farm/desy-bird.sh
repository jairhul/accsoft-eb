#!/bin/bash
#
#
# This is a simple example of a SGE batch script

# request Bourne shell as shell for job
#$ -S /bin/bash

#
# print date and time
echo "$QUEUE $JOB $HOST"

source /etc/profile.d/modules.sh
module use /afs/desy.de/user/i/iagapov/xxl/mpy/colsim/bdsim-rhul/accsoft/modules/all
module load Bdsim/1.1-GCC-4.9.3
source $EBROOTGEANT4/bin/geant4.sh
export ROOT_INCLUDE_PATH=$EBROOTBDSIM/include/bdsim/:$EBROOTBDSIM/include/bdsim/parser/

module list 

bdsim --file=atf2.gmad

# print date and time again
date
