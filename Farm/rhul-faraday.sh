#!/bin/bash

LOCALDIR=/home/sboogert/accsoft/accsoft-eb/Farm/
LOCALOUTPUTDIR=$LOCALDIR/output_$PBS_JOBID/

# make job run dir
JOBDIR=/data/$USER/job_$PBS_JOBID/
mkdir -p $JOBDIR
cd $JOBDIR

cp $LOCALDIR/*gmad $JOBDIR
cp $LOCALDIR/*C    $JOBDIR

echo "------------------"
echo "Module information"
echo "------------------"
module use /home/accsoft/SL68/modules/* 
module avail 2>&1

echo "------------------"
echo "Load gcc"
echo "------------------"
module load GCC
which gcc 
which g++
gcc -v 2>&1

echo "------------------"
echo "Load root         "
echo "------------------"
module load ROOT 
which root 
root -q -b rootTest.C

echo "------------------"
echo "Loaded modules    "
echo "------------------"
module list 2>&1

echo "------------------"
echo "Bdsim             "
echo "------------------"
module load Bdsim
source $EBROOTGEANT4/bin/geant4.sh
bdsim --file=oct.gmad --batch --ngenerate=1000

mkdir -p $LOCALOUTPUTDIR/
cd $LOCALOUTPUTDIR/
cp $JOBDIR/* $LOCALOUTPUTDIR/