#include <iostream>
#include "TFile.h"
#include "TH1.h"

void rootTest() {
  std::cout << "root test" << std::endl;
  TFile *f = new TFile("test.root","RECREATE");
  TH1D *h = new TH1D("h","h",100,-10,10);
  h->FillRandom("gaus",1000);
  f->Write();
  f->Close();
}
