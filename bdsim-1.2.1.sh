echo "Easy Build Module loading BDSIM"
echo "This changes GCC, Geant4, CLHEP, ROOT, Python, IPython"
module use /afs/cern.ch/work/l/lnevay/public/accsoft/modules/*

# must be explicit about version to ensure correct default
module load Bdsim/1.2.1-GCC-4.9.3
source $EBROOTGEANT4/bin/geant4.sh
source $EBROOTROOT/bin/thisroot.sh
export QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb
export QT_QPA_PLATFORM_PLUGIN_PATH=$EBROOTQT5/plugins
export QT_PLUGIN_PATH=$EBROOTQT5/plugins
echo "BDSIM Loaded"

# other useful bits
#module load git
#module load Doxygen
module load Python
module load IPython
module load pymad8/1.4.1-GCC-4.9.3
module load pymadx/1.6.0-GCC-4.9.3
module load pytransport/1.2.1-GCC-4.9.3
module load pybdsim/1.9.1-GCC-4.9.3

module load particletable

# setup BDSIM analysis paths for ROOT
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$EBROOTBDSIM/lib
export ROOT_INCLUDE_PATH=$EBROOTBDSIM/include/bdsim/:$EBROOTBDSIM/include/bdsim/analysis/:$EBROOTBDSIM/include/bdsim/parser/