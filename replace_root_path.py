import os
from glob import iglob


def get_all_directories(base_directory):
    for item in iglob(base_directory + "/*"):
            yield item


def replace_in_file(filename, target, replacement):

    file_contents = []
    with open(filename, "r") as infile:
        for line in infile:
            if target in line:
                line = line.replace(target, replacement)
            file_contents.append(line)

    with open(filename, "w") as outfile:
        for line in file_contents:
            outfile.write(line)


def populate_symlinks(directory, symlink_dict, excluded=None):
    if excluded is None:
        excluded = []

    for root, dirs, files in os.walk(directory):
        for filename in files:
            identifier = os.path.join(os.path.basename(root), filename)
            symlink_dict[identifier] = os.path.abspath(os.path.join(root, filename))
            

def iterate_files(directory, symlink_dict, excluded=None):
    if excluded is None:
        excluded = []

    for root, dirs, files in os.walk(directory):
        populate_symlinks = False
        if os.path.basename(directory) == "all":
            populate_symlinks = True

        for filename in files:
            filepath = os.path.join(root, filename)

            if filename not in excluded:
                if os.path.islink(filepath):
                    os.unlink(filepath)

                    identifier = os.path.join(os.path.basename(root), filename)
                    os.symlink(os.path.relpath(symlink_dict[identifier], root), filepath)

                else:
                    yield filepath


def main():
    top_directory = "."
    target = "/home/accsoft/SL68/software"
    replacement = "/nfs/scratch2/sboogert/SL68/software"

    symlink_dict = {}

    populate_symlinks(os.path.join(top_directory, "modules/all"), symlink_dict)

    for directory in get_all_directories(top_directory):
        for filepath in iterate_files(directory, symlink_dict):
            replace_in_file(filepath, target, replacement)

    print "Done"

if __name__ == "__main__":
    main()
