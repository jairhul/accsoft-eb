import glob as _glob
import shutil as _shutil
from os.path import isfile as _isfile


def Update(oldToolChaninName='GCC',
           oldVersionString='4.9.3',
           newToolChainName='GCC',
           newVersionString='8.3.0-2.32'):
    otc = oldToolChaninName
    ovs = oldVersionString
    ntc = newToolChainName
    nvs = newVersionString

    allFiles = _glob.glob("*.eb")

    for fname in allFiles:
        if otc in fname and ovs in fname:
            fnameNew = fname.replace(otc, ntc)
            fnameNew = fname.replace(ovs, nvs)
            print("Updating " + fname + " to " + fnameNew)
            if _isfile(fnameNew):
                print("Skipping this file as already exists")
                continue

            newtc = "toolchain = {'name': '" + ntc + "', 'version': '"+nvs+"'}\n"
            with open(fname) as f:
                contents = f.readlines()
                newlines = []
                for line in contents:
                    # space after toolchain is crucial because  could be toolchainopts
                    if 'toolchain =' in line:
                        newlines.append(newtc)
                    else:
                        newlines.append(line)
            fo = open(fnameNew, 'w')
            fo.writelines(newlines)
            fo.close()
